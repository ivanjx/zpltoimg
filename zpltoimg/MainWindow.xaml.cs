﻿using BinaryKits.Zpl.Viewer;
using BinaryKits.Zpl.Viewer.Models;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace zpltoimg;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window, INotifyPropertyChanged
{
    const double MM_CONST = 25.4;

    string m_zplStr;
    string m_widthStr;
    string m_heightStr;
    string m_dpiStr;
    ImageSource? m_zplImg;

    public event PropertyChangedEventHandler? PropertyChanged;

    public string ZplStr
    {
        get => m_zplStr;
        set
        {
            m_zplStr = value;
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(ZplStr)));
        }
    }

    public string WidthStr
    {
        get => m_widthStr;
        set
        {
            if (string.IsNullOrEmpty(value))
            {
                m_widthStr = "";
            }
            else if (double.TryParse(value, out double width))
            {
                m_widthStr = width.ToString();
            }

            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(WidthStr)));
        }
    }

    public string HeightStr
    {
        get => m_heightStr;
        set
        {
            if (string.IsNullOrEmpty(value))
            {
                m_heightStr = "";
            }
            else if (double.TryParse(value, out double height))
            {
                m_heightStr = height.ToString();
            }
            
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(HeightStr)));
        }
    }

    public string DpiStr
    {
        get => m_dpiStr;
        set
        {
            if (string.IsNullOrEmpty(value))
            {
                m_dpiStr = "";
            }
            else if (int.TryParse(value, out int dpi))
            {
                m_dpiStr = dpi.ToString();
            }

            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(DpiStr)));
        }
    }

    public ImageSource? ZplImg
    {
        get => m_zplImg;
        set
        {
            m_zplImg = value;
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(ZplImg)));
        }
    }

    public RelayCommand GenerateCommand
    {
        get => new RelayCommand(Generate);
    }

    public MainWindow()
    {
        InitializeComponent();
        DataContext = this;

        m_zplStr = "";
        m_widthStr = "4";
        m_heightStr = "6";
        m_dpiStr = "8";
    }

    void Generate()
    {
        ZplImg = null;
        ZplStr = ZplStr.Trim();

        if (string.IsNullOrEmpty(ZplStr))
        {
            return;
        }

        try
        {
            IPrinterStorage printerStorage = new PrinterStorage();
            ZplElementDrawer drawer = new ZplElementDrawer(printerStorage);

            ZplAnalyzer analyzer = new ZplAnalyzer(printerStorage);
            AnalyzeInfo analyzeInfo = analyzer.Analyze(ZplStr);

            if (analyzeInfo.LabelInfos.Length != 1)
            {
                throw new Exception("Invalid labels count");
            }

            double width = double.Parse(WidthStr) * MM_CONST;
            double height = double.Parse(HeightStr) * MM_CONST;
            int dpi = int.Parse(DpiStr);
            byte[] imageBuff = drawer.Draw(
                analyzeInfo.LabelInfos[0].ZplElements,
                width,
                height,
                dpi);

            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = new MemoryStream(imageBuff);
            image.CacheOption = BitmapCacheOption.OnLoad;
            image.EndInit();
            image.Freeze();
            ZplImg = image;
        }
        catch (Exception ex)
        {
            MessageBox.Show(
                "Error: " + ex.Message,
                "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
        }
    }
}
